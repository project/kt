
/*
 * The kt javascript add the necessary javascript function to view KnowledgeTree in an iframe
 */
 
function showiframe() { 
	var ktnode = document.getElementById("kt_iframe"); 
	ktnode.style.display = "block"; 
	var ktnode = document.getElementById("kt_loading"); 
	ktnode.style.display = "none"; 
} 

function setiframeheight() {
	var extraHeight=32; //extra height in px to add to iframe 
	dyniframe = document.getElementById("kt_iframe");
	
	if (dyniframe && !window.opera){
		dyniframe.style.display="block"
		
		if (dyniframe.contentDocument && dyniframe.contentDocument.body.offsetHeight){ //ns6 syntax
			dyniframe.height = dyniframe.contentDocument.body.offsetHeight+extraHeight;
		} else if (dyniframe.Document && dyniframe.Document.body.scrollHeight){ //ie5+ syntax
			dyniframe.height = dyniframe.Document.body.scrollHeight+extraHeight;
		}
	}		
	
	// This adds an eventlister to the body of knowledge tree so that any cick checks the size of the doc. It has to fire one millisecond later to take acount of any changes
	if (dyniframe.contentDocument && dyniframe.contentDocument.body.offsetHeight) {
		dyniframe.contentDocument.body.addEventListener('click',function (e) {
			var t=setTimeout(function () {
				var extraHeight=32; //extra height in px to add to iframe 
				dyniframe = parent.document.getElementById('kt_iframe');
			
				if (dyniframe && !window.opera){
					dyniframe.height = dyniframe.contentDocument.body.offsetHeight+extraHeight;
				}
			} ,1);
		},true);
	} else if (dyniframe.Document && dyniframe.Document.body.scrollHeight) {
		dyniframe.Document.body.onclick=function() {
			var t=setTimeout(function () {
				var extraHeight=32; //extra height in px to add to iframe 
				dyniframe = parent.document.getElementById('kt_iframe');
				if (dyniframe && !window.opera){
					dyniframe.height = dyniframe.Document.body.scrollHeight+extraHeight;
				}
			} ,5);
			
		};
	}
}
		
function changetext(text) {
	divi = document.getElementById("width_link");
	divi.innerHTML = text;
}