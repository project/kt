<?php
/**
 *
 * The contents of this file are subject to the KnowledgeTree Public
 * License Version 1.1.2 ("License"); You may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.knowledgetree.com/KPL
 * 
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * See the License for the specific language governing rights and
 * limitations under the License.
 *
 * All copies of the Covered Code must include on each user interface screen:
 *    (i) the "Powered by KnowledgeTree" logo and
 *    (ii) the KnowledgeTree copyright notice
 * in the same form as they appear in the distribution.  See the License for
 * requirements.
 * 
 * The Original Code is: KnowledgeTree Open Source
 * 
 * The Initial Developer of the Original Code is The Jam Warehouse Software
 * (Pty) Ltd, trading as KnowledgeTree.
 * Portions created by The Jam Warehouse Software (Pty) Ltd are Copyright
 * (C) 2007 The Jam Warehouse Software (Pty) Ltd;
 * All Rights Reserved.
 * Contributor( s): ______________________________________
 *
 */

require_once("../config/dmsDefaults.php");
require_once(KT_LIB_DIR . '/authentication/authenticationutil.inc.php');
require_once(KT_LIB_DIR . '/session/Session.inc');

class KTLogoutDispatcher extends KTDispatcher {
    function do_main() {
        global $default;

        /*$oAuthenticator =& KTAuthenticationUtil::getAuthenticatorForUser($this->oUser);
        $oAuthenticator->logout($this->oUser);*/

		Session::destroy();
// Drupal is putting Knowledge tree into an iframe therefore need to use javascript to redirect the parent window to Drupal's logout
//        redirect((strlen($default->logoutUrl) > 0 ? $default->logoutUrl : "/"));
		echo ('<script language="javascript" type="text/javascript">
               <!--
                  window.parent.location.replace(
                    "' .(strlen($default->drupalURL . $default->logoutURL) > 0 ? $default->drupalURL . $default->logoutURL : "/") .'");
               -->
               </script>');
        exit(0);
    }
}
$d =& new KTLogoutDispatcher;
$d->dispatch();
?>
