<?php
// DrupalPlugin.php
// This interceptor requires the KnowledgeTree module to be installed in the Drupal Installation to work correctly

require_once(KT_LIB_DIR . '/plugins/plugin.inc.php');
require_once(KT_LIB_DIR . '/plugins/pluginregistry.inc.php');
require_once(KT_LIB_DIR . '/authentication/interceptor.inc.php');
require_once(KT_LIB_DIR . '/authentication/interceptorregistry.inc.php');
require_once(KT_LIB_DIR . '/groups/Group.inc');
require_once(KT_DIR . '/thirdparty/xmlrpc-2.2/lib/xmlrpc.inc');

class DrupalInterceptor extends KTInterceptor {
	var $sNamespace  = "drupal.drupal.interceptor";

	function setupdrupalxmlrpc() {
		$oKTConfig = KTConfig::getSingleton();
		$drupalXmlrpc = $oKTConfig->get('drupalXmlrpc', false);
		if ($drupalXmlrpc === false){
			return PEAR::raiseError(_kt('No config for Drupal XMLRPC server'));
		}
		$oKTConfig = KTConfig::getSingleton();
		$drupalURL = $oKTConfig->get('drupalURL', false);
		$client = new xmlrpc_client($drupalURL . $drupalXmlrpc);
		$client->return_type = 'phpvals';
		return($client);
	}
	
	function authenticated() {

		//Get Drupal's seesion name to see if user is logged in
		$client = $this->setupdrupalxmlrpc();
		if (PEAR::isError($client)) {
            return $client;
        }
		$message = new xmlrpcmsg("kt.getSessionName");
		$r=&$client->send($message);
		$drupal_session_name = kt_decryptData($r->val);
	  if (!isset($_COOKIE[$drupal_session_name])) {
      print 'The session name return from XMLRPC call ('. $drupal_session_name .') does not match current session name ('. $_COOKIE[session_name()] .'). Modify settings.php according to instructions found in Knowledge Tree\'s admin page in Drupal. Or if you see strange character in the returned session name check that your secret keys match';
			return;
		}

		//Get the user from the Drupal Database
		$sid = $_COOKIE[$drupal_session_name];
		$message = new xmlrpcmsg("kt.getUserBySession",array(php_xmlrpc_encode(kt_encryptData($sid))));
		$r=&$client->send($message);
		$duser = unserialize(kt_decryptData($r->val));

		IF (!($duser && $duser->uid > 0)) {
      print "Could not retreive valid user information from Drupal";
			setcookie($drupal_session_name, "", time() - 86400);
			unset($_COOKIE[$drupal_session_name]);
			return;
		}

		$oUser =& User::getByUsername($duser->name);

		//If there is a profile field called profile_fullname use it in KnowledgeTree
		if (empty($duser->profile_fullname)){
			$userfullname = $duser->name;
		} else {
			$userfullname = $duser->profile_fullname;
		}


		if (PEAR::isError($oUser) || ($oUser === false)) {
			$oUser =& User::createFromArray(array(
				"Username" => $duser->name,
				"Name" => $userfullname,
				"Email" => $duser->mail,
				"EmailNotification" => true,
				"SmsNotification" => false,
				"MaxSessions" => 3,
				"password" => $duser->pass,
			));
			// This is a good place to do any additional tasks,
			// such as creating a home folder or adding the user to roles/groups.
		} else {
		    // Update user with new info from Drupal
			$oUser->setName($userfullname);
			$oUser->setEmail($duser->mail);
			$oUser->setPassword($duser->pass);
		}
		
		if ($duser->kt_copyusersandroles){
		// Add user to same roles in KnowledgeTree as they have in Drupal
			foreach ($duser->roles as $key => $role){
				$oGroup = Group::createFromArray(array('name'=>$role));
				$oGroup = Group::getByName($role);
				$oGroup->addMember($oUser);
			}
		}
		
		//If  the user has admin rights from Drupal's user tables - assumes KT system admins has a group id = 1
		if ($duser->kt_admin) {
			$oGroup = Group::get(1);
			$oGroup->addMember($oUser);
		}
		
		return $oUser;
	}

	function takeover() {
		//Get Drupal's seesion name to see if user is logged in
		$client = $this->setupdrupalxmlrpc();
		if (PEAR::isError($client)) {
            return $client;
        }
		$message = new xmlrpcmsg("kt.getSessionName");
		$r=&$client->send($message);
		$drupal_session_name = kt_decryptData($r->val);

		// Need to strip out directories if drupal installation is not root 
		// so that the drupal login redirector will forward user to the correct page
		$oKTConfig = KTConfig::getSingleton();
		$rootUrl = $oKTConfig->get('rootUrl', '/');
		preg_match('/(.*\/)(.*)$/', $rootUrl, $output);
		$basepath = $output[1];
		
		// Re-encode url so that it can be passed through the redirectors of both Drupal and KnowledgeTree
		if (strpos($_SERVER["REQUEST_URI"], "redirect")>0){
          $urlarray = split('redirect\=|\&errorMessage',urldecode($_SERVER["REQUEST_URI"]));
	      $ref = urldecode($urlarray[1]);
	      $rUrl = parse_url($ref);
          if (strpos($rUrl['query'], "qs=")>0){
            $pvars = split('qs\=',$rUrl['query']);
	        $pvars = split('\&',$pvars[1]);
	        $pvars = $pvars[0];
	        $ref = str_replace($pvars,urlencode($pvars),$ref);
          }
        } else {
	      $ref = $rootUrl . "/login.php";
        }

		//Remove drupal directory if it exists
		$ref = substr($ref, strlen($basepath));
		$oKTConfig = KTConfig::getSingleton();
		$drupalURL = $oKTConfig->get('drupalURL', false);
		if (!isset($_COOKIE[$drupal_session_name])) {
			header($drupalURL.'user?destination='.urlencode($ref));
			exit();
		}
		
	}
}

class DrupalPlugin extends KTPlugin {
	var $sNamespace = "drupal.drupal.plugin";
	var $autoRegister = true;
	var $bAlwaysInclude = true;

	function DrupalPlugin($sFilename = null) {
		$res = parent::KTPlugin($sFilename);
		$this->sFriendlyName = _kt("Drupal Login Plugin");
		return $res;
	}

	function setup() {
		$this->registerInterceptor("DrupalInterceptor", "drupal.drupal.interceptor", __FILE__);
	}
}

function kt_decryptData($value){ 
  $oKTConfig = KTConfig::getSingleton();
  $key = $oKTConfig->get('drupalSecretKey', false); 

  
  if ($key && extension_loaded("mcrypt")) {
    $crypttext = base64_decode($value); 
    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB); 
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND); 
    $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $crypttext, MCRYPT_MODE_ECB, $iv); 
  }
  else {
    $decrypttext = $value;
  }
  return trim($decrypttext); 
} 

function kt_encryptData($value){ 
  $oKTConfig = KTConfig::getSingleton();
  $key = $oKTConfig->get('drupalSecretKey', false); 
  if ($key && extension_loaded("mcrypt")) {
   $text = $value; 
   $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB); 
   $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND); 
   $crypttext = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $text, MCRYPT_MODE_ECB, $iv)); 
  }
  else {
    $crypttext = $value;
  }
  return $crypttext; 
} 

$oRegistry =& KTPluginRegistry::getSingleton();
$oRegistry->registerPlugin('DrupalPlugin','drupal.drupal.plugin', __FILE__);
?>