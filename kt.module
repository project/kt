<?php

/*
 * @file
 * The kt module provides a way to integrate the Knowledge tree DMS with Drupal
 */

/**
 * Implementation of hook_help().
 */
function kt_help($path, $arg) {
  switch ($path) {
    case 'admin/help#kt':
      return '<p>'. t('The kt module provides a way to integrate the KnowledgeTree DMS with Drupal') .'</p>';
  }
}
/**
 * Implementation of hook_menu().
 */
function kt_menu() {

  $items['dms'] = array(
    'title' => 'KnowledgeTree Document Management System',
    'page callback' => 'kt_page',
    'access arguments' => array('login to kt'),
  );
  $items['admin/settings/kt'] = array(
    'title' => 'KnowledgeTree settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('kt_admin_settings'),
    'access arguments' => array('administer kt'),
  );

  return $items;

}

/**
 * Implementation of hook_perm().
 */
function kt_perm() {
  return array("administer kt", "login to kt");
}

// Displays the kt settings page
function kt_admin_settings() {

  $form['help'] = array(
    '#type' => 'fieldset',
    '#title' => t('KT help')
  );

  $form['help']['basic'] = array(
    '#type' => 'markup',
    '#value' => '<p>This module will not do anything unless you have a working install of <a href="http://www.knowledgetree.com/">Knowledge Tree Document Management System</a>. The open source version is available from <a href="http://sourceforge.net/">Sourceforge</a></p>',
  );

  $form['help']['adv'] = array(
    '#type' => 'fieldset',
    '#title' => t('Install instructions'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['help']['adv']['text'] = array(
    '#type' => 'markup',
    '#value' => "<p>Installing the KnowledgeTree module on Drupal and Knowledge tree.</p>
<ol><li>Add lines from kt_config.ini (in the knowledgeTree folder in the module) to config.ini in the config folder in your KnowledgeTree install.</li>
<li>Copy the drupal folder (in the KnowledgeTree folder in the module) into the plugins directory in your KnowledgeTree install</li>
<li>If the thirdparty folder in your KnowledgeTree directory does not contain a folder called xmlrpc-2.2, <a href=\"http://sourceforge.net/projects/phpxmlrpc/files/\">download it from here</a>, create a directory called xmlrpc-2.2 and extract contents to this directory</li>
<li>Check that the session names match. There will be a drupal message about this in the status log and KnowledgeTree settings page.<br/><br/>
IF they do not you need to alter the settings.php in the sites/default folder (if you are using drupal in a multi-domain set up this might not be true) and set the cookie domain by removing the comment from the beginning of this line:
<blockquote># \$cookie_domain = 'example.com';</blockquote>
and setting the value correctly for your site.
<li>Fill out the administration form in site configuration for KnowledgeTree.</li>
<li>Run the following SQL on the KnowledgeTree database:
<blockquote>INSERT INTO interceptor_instances (id, name, interceptor_namespace) VALUES (1, 'DrupalInterceptor', 'drupal.drupal.interceptor');</blockquote>
<blockquote><em>On more recent versions of Knowledge Tree you might have to use an id of 2</em></blockquote>
</li>
<li>Copy the logout.php in the knowledgeTree folder in the module) into the presentation directory in your KnowledgeTree install.
<blockquote><em>The version of logout.php supplied is known to work with Knowledge Tree 3.4 - for more recent versions: the purpose of this change is to replace Knowledge Tree's redirect with a javascript snippet to log the user out from Drupal too.</em></blockquote>
</li>
<li>Login in to your KnowledgeTree installation and enable the Drupal plugin in the DMS administration section -> Miscellaneous -> Manage Plugins (you might have to click on the Reread Plugins button)</li>
<li>If you want the KnowledgeTree pages to always load with the iframe you need to edit the standard_page.smarty in the templates/kt3 folder in your knowledgeTree install). Add the following lines just before the &lt;/head&gt; tag. <br /><br />
&lt;!-- Little script to force KT into drupal --&gt;<br />
&lt;script type=\"text/javascript\"&gt; <br />
  if (self.location == top.location &amp;&amp; location.search.substring(1) != 'nf') <br />
    top.location.href = '../DMS?kt_page=' + escape(self.location); <br />
&lt;/script&gt;</li>
<li>To find out more about interceptors and KnowledgeTree visit <a href=\"http://wiki.knowledgetree.com/Using_Interceptors_to_Override_KnowledgeTree's_Login_Process\">http://wiki.knowledgetree.com/Using_Interceptors_to_Override_KnowledgeTree's_Login_Process</a></li></ul>",
  );

  $form['kt_path'] = array(
    '#type' => 'textfield',
    '#title' => t("Path"),
    '#default_value' => variable_get("kt_path", "kt/login.php"),
    '#size' => '50',
    '#maxlength' => '100',
    '#description' => t("Path to the KnowledgeTree login page (absolute or relative).  Use a trailing slash.")
  );
  $form['kt_width'] = array(
    '#type' => 'textfield',
    '#title' => t("IFrame width"),
    '#default_value' => variable_get("kt_width", "100%"),
    '#size' => '6',
    '#maxlength' => '6',
    '#description' => t("Sets the width of the IFrame. Enter a number and units (px, %, em). E.g. 100%")
  );
  $form['kt_height'] = array(
    '#type' => 'textfield',
    '#title' => t("IFrame height"),
    '#default_value' => variable_get("kt_height", "auto"),
    '#size' => '6', '#maxlength' => '6',
    '#description' => t("Sets the height of the IFrame. Enter a number and units (px, %, em). E.g. 600px OR you can enter auto and the iframe will resize dynamically")
  );
  $form['kt_scroll'] = array(
    '#type' => 'select',
    '#title' => t('Scrolling IFrame'),
    '#default_value' => variable_get("kt_scroll", TRUE),
    '#options' => array(FALSE => "No", TRUE => "Yes"),
    '#description' => t("Should scrollbars be displayed if KnowledgeTree is too large to fit in the IFrame.")
  );
  $form['kt_copyusersandroles'] = array(
    '#type' => 'select',
    '#title' => t('Copy Drupal users and roles to KT'),
    '#default_value' => variable_get("kt_copyusersandroles", TRUE),
    '#options' => array(FALSE => "No", TRUE => "Yes"),
    '#description' => t("Should KnowledgeTree automatically copy drupal user and roles when logging on.")
  );
  $form['kt_disable_blocks'] = array(
    '#type' => 'select',
    '#title' => t('Disable sidebar blocks'),
    '#default_value' => variable_get("kt_disable_blocks", FALSE),
    '#options' => array(FALSE => "No", TRUE => "Yes"),
    '#description' => t("Disable the left and right regions on the KT document management page.")
  );
  if (extension_loaded("mcrypt")) {
    $form['kt_secret_key'] = array(
      '#type' => 'textfield',
      '#title' => t("Secret Key"),
      '#default_value' => variable_get("kt_secret_key", ""),
      '#size' => '60', 
      '#maxlength' => '128',
      '#description' => t("Sets the secret key used to secure communications between Knowledge Tree and Drupal. If this is set you must set the drupalSecretKey value in Knowledge tree's config.ini")
    ); 
  }
  else {
    drupal_set_message(t('It is recommended that you enable/install PHP\'s mcrypt library to secure communications between Drupal and Knowledge Tree. More information <a href="http://www.php.net/mcrypt">http://www.php.net/mcrypt</a>'), 'warning');
  }
  return system_settings_form($form);
}

/*
 * Function creates iframe to load KnowledgeTree page into
 */

function kt_page() {
  $path = variable_get("kt_path", "kt/login.php");
  $width = variable_get("kt_width", "100%");
  $height = variable_get("kt_height", "auto");
  $scrolling = variable_get("kt_scroll", TRUE);
  if ($_GET['kt_page']) {
    $path = $_GET['kt_page'];
  }

  //Add required javascript file
  $modulepath = drupal_get_path('module', 'kt');
  drupal_add_js($modulepath .'/kt.js');
  //Create iframe
  $output .= '<iframe src="'. $path .'" id="kt_iframe" frameborder="0" onload="showiframe();';

  if ($height=="auto") {
    $output .= 'setiframeheight();';
  }
  $output .= '"';

  if (!$scrolling||$height=="auto") {
    $output .= ' scrolling="no"';
  }

  if ($width && $height) {
    $output .= ' style="width: '. $width .'; display:none;"';
  }
  $output .= '></div><p>Sorry your browser doesn\'t support iframes\'s.</p></iframe>';
  $output .= '<div id="kt_loading"><p>Loading document manager...</p></div>';
  if (variable_get("kt_disable_blocks", FALSE)) {
    print theme('page', $output, FALSE);
  }
  else {
    return $output;
  }
}

/*
 * Implementation of hook_xmlrpc().
 */
function kt_xmlrpc() {
  return array('kt.getUserBySession' => 'kt_get_user_by_session', 'kt.getSessionName' => 'kt_get_session_name');
}

/*
 * XMLRPC function to return Drupal's session name
 */
function kt_get_session_name() {
  return kt_encryptData(session_name());
}

/*
 * XMLRPC function to find user based on session id
 */
function kt_get_user_by_session($sess) {
  //global $user;
  $sess = kt_decryptData($sess);
  $user = db_fetch_object(db_query("SELECT u.uid FROM {users} u INNER JOIN {sessions} s ON u.uid = s.uid WHERE s.sid = '%s'", $sess));
  $uids = array();
  $uids['uid'] = $user->uid;
  $user = user_load($uids);
  if ($user && $user->uid > 0) {
    // This is done to unserialize the data member of $user
    //$user = drupal_unpack($user);
    if (user_access('login to kt', $user)) {
      //Add extra information to user object that is needed to make the KnowledgeTree Drupal Interceptor work
      $user->kt_copyusersandroles = variable_get('kt_copyusersandroles', TRUE);
      $user->kt_admin = user_access('administer kt', $user);
      return kt_encryptData(serialize($user));
    }
    else {
      //No permissions
      return xmlrpc_error(1, t('Access denied '. $user->uid));
    }
  }
  else {
    //return error not logged in
    return xmlrpc_error(1, t('No user logged in'));
  }
}

function kt_encryptData($value) { 
  $key = variable_get("kt_secret_key", "");
  if ($key && extension_loaded("mcrypt")) {
    $text = $value; 
    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB); 
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND); 
    $crypttext = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $text, MCRYPT_MODE_ECB, $iv)); 
  }
  else {
    $crypttext = $value;
  }
  return $crypttext; 
} 

function kt_decryptData($value) { 
  $key = variable_get("kt_secret_key", "");
  if ($key && extension_loaded("mcrypt")) {
    $crypttext = base64_decode($value); 
    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB); 
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND); 
    $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $crypttext, MCRYPT_MODE_ECB, $iv); 
  }
  else {
    $decrypttext = $value;
  }
  return trim($decrypttext); 
} 

